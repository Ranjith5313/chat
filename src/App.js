import React from 'react';
import logo from './logo.svg';
import './App.css';
import Chat from './Components/chat'

function App() {
  return (
    <div className="App">
     <Chat/>
    </div>
  );
}

export default App;
