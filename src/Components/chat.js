import React, { Component,useState,useEffect,useRef,useCallback } from "react";
import { GiftedChat,InputToolbar,Composer,Send } from "react-web-gifted-chat";
import moment from 'moment'
import './chat.css'
import {Row,Col,Input,Modal,ModalBody,Button,ModalHeader,Card,CardBody,CardHeader} from 'reactstrap'

function Chat(props) {
  
  const [Messages,setMessages] = useState([])
  const [send,setSend] = useState(false)
  const [messageText,setMessageText] = useState("")
  const onSendMessage =(message)=> {
    let chats = Messages
    chats.push({ id: message[0].id, content: message[0].text, user: message[0].user.id,image:image })
    setMessages(chats)
    setSend(!send)
    setMessageText("")
    setImage("")
  }

  const [image,setImage] = useState("")
  const [modalvisible,setModalVisible] = useState(false)
  const [isLoading,setIsLoading] = useState(false)
  const [keyWord,setKeyWord] = useState("")
  const [user,setUser] = useState([])

  useEffect(()=>{
    setMessages([{ id: 2, content: "That's it. That's all there is.", user: "sam" },
    { id: 1, content: "Six by nine. Forty two.", user: "me" },
    {
      id: 0,
      image: 'https://img.alicdn.com/tfs/TB1ZZJNB.Y1gK0jSZFCXXcwqXXa-990-400.png',
      content:
        "What's the angle of the red light that refracts through a water surface to create a 🌈?",
      user: "anonymous"
    }])
  },[])

  const msg = useRef(null)

  const customtInputToolbar = props => {
    return (
      <InputToolbar
        {...props}
        containerStyle={{
          backgroundColor: "white",
          borderTopColor: "#E8E8E8",
          borderTopWidth: 1,
          padding: 8
        }}
      />
    );
  };

  const changeImage = event => {
    setImage(URL.createObjectURL(event.target.files[0]))
}

  useEffect(()=>{
    console.log("messages...",Messages)
  },[send])

  useEffect(()=>{
    onUserMention()
  },[keyWord])

  const renderComponser = (props) => {
    console.log("props...",props)
    return (
      <div style={styles.composerContainer}>
        
          {modalvisible &&<div style={styles.suggestion}>
            <Card>
            
              {user.map((data)=>{
                  return <CardBody>
                    {data.user}
                  </CardBody>
                })}
            
          </Card>
          </div> }
          <div style={styles.inputContainer}>
          <Input {...props}
          className="message"
          type="textarea"
            placeholder={'Type something...'}
            ref={msg}
            onChange={(value) => onTextChange(value.target.value, props)}
            style={styles.textInput}
            value={messageText}
            multiline={true}
          />
        </div>
      </div>
    )
  }

  const onTextChange = (value, props) => {
    // return console.log("value...",value)
    const lastChar = messageText.substr(messageText.length - 1)
    const currentChar = value.substr(value.length - 1)
    const spaceCheck = /[^@A-Za-z_]/g
    props.onTextChanged(value)
    setMessageText(value)
    // this.setState({
    //   messageText: value
    // })
    if(value.length === 0) {
      setModalVisible(false)
    } else {
      if (spaceCheck.test(lastChar) && currentChar != '@') {
        setModalVisible(false)
      } else {
        const checkSpecialChar = currentChar.match(/[^@A-Za-z_]/)
        if (checkSpecialChar === null || currentChar === '@') {
          const pattern = new RegExp(`\\B@[a-z0-9_-]+|\\B@`, `gi`);
          const matches = value.match(pattern) || []
          if (matches.length > 0) {
            getUserSuggestions(matches[matches.length - 1])
            setModalVisible(true)
          } else {
            setModalVisible(false)
          }
        } else if (checkSpecialChar != null) {
          setModalVisible(false)
        }
      }
    }
  }

const onUserMention = ( ) => {
        
        if(Array.isArray(Messages)) {
          
                if(keyWord.slice(1) === '') {
                  // return alert(keyWord)
                  // setMessages(Messages)
                  setUser(Messages)
                  setIsLoading(false)
                  
                } else {

                  const userDataList = Messages.filter(obj => obj.user.indexOf(keyWord.slice(1)) !== -1)
                  // setMessages(userDataList)
                  setUser(userDataList)
                  setIsLoading(false)
                }
              } 
          setIsLoading(true);

      }
 
  const getUserSuggestions = (keyword) => {

    setKeyWord(keyword)
    
    return onUserMention
     
  }

  const renderAccessory = () =>{
    if(image)return <img src={image} height={100} width={100} />
  }

    return (
      <div style={styles.container}>
         <GiftedChat
          messages={Messages.slice().reverse().map(m => ({
            id: m.id,
            text: m.content,
            user: {
              // id: m.user,
              name: m.user
            },
            image:m.image && m.image
          }))}
          onSend={message => onSendMessage(message)}
          user={{
            id: "me"
          }}
          text={messageText}
          alwaysShowSend={true}
          minComposerHeight={55}
          maxComposerHeight={55}
          renderAccessory={()=>renderAccessory()}
          renderTime={moment()}
          showAvatarForEveryMessage={true}
          // renderInputToolbar={props => customtInputToolbar(props)}
          // onPressAvatar={()=>alert("hi")}
          renderComposer={(props)=>renderComponser(props)}
          loadEarlier={true}
          onLoadEarlier={()=>alert("hi")}
          // isLoadingEarlier={true}
          renderActions={(props) => (
            <Row >
              {/* <BtnRound icon="camera" iconColor={Colors.primaryBlue} size={40} style={{ marginHorizontal: 5 }} onPress={() => choosePicture()} />
              <Send {...props}>
                <View style={styles.btnSend}>
                  <Icon name="ios-send" size={24} color="#ffffff" />
                </View>

              </Send> */}<Col lg="6" >
                </Col>
              <Col lg="3" >
              <input type="file" name="file-5[]" id="file-5" class="inputfile inputfile-4" onChange={(evt)=>changeImage(evt)} data-multiple-caption="{count} files selected" multiple />
					    <label for="file-5"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure></label>
                </Col>
                
              </Row>
          )}
        />
      </div>
      
    );
  }

const styles = {
  container: {
    flex: 1,
    height: "100vh"
  },
  inputContainer:{
    maxWidth:"100vh"
  },
  suggestion:{
    margin: "-80px 0 0 0",
    background: "white",
    textAlign: "initial",
    padding: "8px",
    height:"80px",
    overflow:"auto"
  }
};

export default Chat;